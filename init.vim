" nvim configuration ~~~ Jean-Baptiste Bourgoin 2020
" inspirations : https://vimawesome.com/
" reload conf :so $MYVIMRC
" Leader key  : /

set nocompatible
set mouse=a

" VISUAL
" ====================================================================
colorscheme gruvbox
let g:lightline_theme='gruvbox'
set number

" Vertical panes
" ====================================================================
" Open NerdTree : ':NE'
" Open Taglist : ':TL'
:command TL Tlist

" VIM-PLUG
" ====================================================================

" Reload .vimrc and :PlugInstall to install plugins.
" :PlugUpdate to update
" If you want to delete a plugin, remove the line from your config. Quit and restart Vim, and then run :PlugClean

" plugin installation folder :
call plug#begin('~/.vim/plugged')

" Lightline
Plug 'https://github.com/itchyny/lightline.vim'

" pico 8 related :
" Plug 'https://github.com/justinj/vim-pico8-syntax'
Plug 'https://github.com/aquova/vim-pico8-syntax'

" markdown
Plug 'godlygeek/tabular'
Plug 'https://github.com/plasticboy/vim-markdown'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" surround
" '→ # : cs'#
Plug 'https://github.com/tpope/vim-surround'

" gitgutter - git interface
" You can jump between hunks with [c and ]c.
" preview hunks : <leader>hp
" stage hunks : <leader>hs
" undo hunks : <leader>hu respectively.
Plug 'https://github.com/airblade/vim-gitgutter'

" ALE
Plug 'https://github.com/dense-analysis/ale'

" Tag List
Plug 'https://github.com/vim-scripts/taglist.vim'

" NERD commenter
" Leader+cc : comment
" Leader+cn : uncomment
Plug 'http://github.com/scrooloose/nerdcommenter'

" Plugin system init
call plug#end()

